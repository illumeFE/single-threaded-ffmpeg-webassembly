const readFromBlobOrFile = (blob) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.onerror = ({
      target: {
        error: { code },
      },
    }) => {
      reject(Error(`File could not be read! Code=${code}`));
    };
    fileReader.readAsArrayBuffer(blob);
  });
};

const getCore = async () => {
  const resolves = {};
  let resolveExit = null;
  let _id = 0;
  const getID = () => _id++;
  const worker = new Worker('./worker.js');

  const getHandler = (payload) =>
    new Promise((resolve) => {
      const id = getID();
      worker.postMessage({ id, ...payload });
      resolves[id] = resolve;
    });

  worker.onmessage = (e) => {
    const { id, data } = e.data;
    resolves[id](data);
  };

  await getHandler({ type: 'INIT' });

  return {
    getHandler,
    exit: () =>
      new Promise((resolve) => {
        worker.terminate();
        resolveExit = resolve;
        resolveExit();
      }),
    FS: ['mkdir', 'writeFile', 'readFile'].reduce((acc, cmd) => {
      acc[cmd] = (...args) => getHandler({ type: 'FS', cmd, args });
      return acc;
    }, {}),
  };
};

const ffmpeg = ({ core, args }) => core.getHandler({ type: 'RUN', args });

const runFFmpeg = async (inputFileName, data, args, outputFileName) => {
  const core = await getCore();
  await core.FS.writeFile(inputFileName, data);
  await ffmpeg({ core, args });
  const file = await core.FS.readFile(outputFileName);
  try {
    await core.exit();
  } catch (e) {}
  return { file };
};
