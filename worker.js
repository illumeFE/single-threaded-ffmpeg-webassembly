importScripts('./parseArgs.js');
importScripts('./ffmpeg-core.js');
let core = null;

onmessage = async (event) => {
  const { id, type, cmd, args } = event.data;
  console.log({ id, type, cmd, args });
  switch (type) {
    case 'INIT':
      console.log('initializing', core);
      core = await createFFmpegCore({
        printErr: () => {},
        print: () => {},
      });
      console.log('finish init', core);
      postMessage({ id, type: 'INIT' });
      break;
    case 'FS':
      const blocklist = ['mkdir'];
      const data = core.FS[cmd](...args);
      postMessage({
        id,
        type: 'FS',
        data: !blocklist.includes(cmd) ? data : null,
      });
      break;
    case 'RUN':
      try {
        core.ccall(
          'main',
          'number',
          ['number', 'number'],
          parseArgs(core, ['ffmpeg', '-hide_banner', '-nostdin', ...args])
        );
      } catch (e) {}
      postMessage({ id, type: 'RUN' });
      break;
    default:
      console.log('unknown message type: ', type);
  }
};
